package com.mochizuki.tetris;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;

import com.badlogic.gdx.backends.android.AndroidApplication;

public class MainActivity extends AndroidApplication {

	private MainScreen m_mainScreen;
	private MenuScreen m_MenuScreen;
	private AppGame m_Game;
	private boolean m_CanResume;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_main);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		m_mainScreen = new MainScreen(this);
		m_MenuScreen=new MenuScreen(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		//m_mainScreen.pause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		m_mainScreen.resume();
		super.onResume();
		//m_Game.setScreen(m_mainScreen);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		SharedPreferences shp = getSharedPreferences("state", Activity.MODE_PRIVATE);
		if(shp.contains("current"))
			m_CanResume=true;
		else m_CanResume=false;
		
		m_Game = new AppGame(m_MenuScreen);
		initialize(m_Game, true);
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	
	public void toMenu(){
		m_Game.setScreen(m_MenuScreen);
	}
	public void toMainScreen(boolean isnew){
		if(!m_CanResume)
			m_mainScreen.SetGameState(true);
		else m_mainScreen.SetGameState(isnew);
		m_Game.setScreen(m_mainScreen);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		//m_mainScreen.SaveState(outState);
		Log.i(this.getClass().getSimpleName(), "onSaveInstanceState..........");
		super.onSaveInstanceState(outState);
	}

}
